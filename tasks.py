from invoke import task
from os import environ

@task
def install_dependencies(c):
    """ Install ansible dependencies for development. """
    c.run("pip install -r requirements.txt")

@task
def docs(c):
    c.run("mkdocs serve")


@task
def build_ci_image(c):
    """ Build CI docker image. """
    c.run("docker build -t deploy-cda-sso -f ci/Dockerfile .")

@task
def test(c):
    """ Run the tests. """
    c.run("molecule test")


@task
def example(c):
    """ Run playbook. """
    environ["ANSIBLE_ROLES_PATH"] = "../"
    c.run("ansible-playbook -i example/inventory example/site.yml")
