# Done in first commit

## Traefik

- [x] Traefik deployment
- [x] Configure docker image
- [x] Configure domain
- [x] Configure subdomain
- [x] Configure HTTP/HTTPS
- [x] Configure TLS certificates
- [x] Configure Let's Encrypt
- [x] Persistent Storage for certificates renewal
- [x] Configure Datadog tracing
- [x] Configure Datadog metrics
- [x] Configure resource allocation
- [x] Configure logging
- [x] Configure SSO authorization

## Keycloak

- [x] Keycloak deployment
- [x] Configure Docker image
- [x] Configure domain
- [x] Configure subdomain
- [x] Configure resource allocation
- [x] Configure logging

## Keycloak Realms

- [x] Keycloak Configuration
- [x] Automatically create CDA Realm
- [x] Automatically create SSO client
- [x] Automatically fetch SSO client secret
- [x] Automatically create admin user for CDA realm
- [x] Automatically define password of admin user for CDA realm

## Traefik Forward Auth

- [x] Traefik-forward-auth deployment
- [x] Configure Docker image
- [x] Configure resource allocation
- [x] Configure logging
- [x] Automatically configure OIDC Issuer URL, OIDC Client ID and OIDC Client Secret

## Tests

- [x] Code quality test on Ansible scripts
- [x] Test that deployment converge
- [x] Test idempotence
- [x] Test structure

# TO DO

## Keycloak

- [ ] `IN PROGRESS`: Configure LDAP User Federation
- [ ] `TODO`: Configure Kerberos (SPENEGO mechanism)
- [ ] `TODO`: Configure PostgreSQL persistent storage
- [ ] `TODO`: Provide Ansible task/playbook to perform realms backups/restore

## Traefik

- [ ] `TODO`: Configure PostgreSQL persistent storage

## Traefik Forward Auth

- [ ] `TODO`: Check X-Forwared-User header presence

## Tests

- [ ] `TODO`: Test status code when authenticated or not
- [ ] `TODO`: Load test to estimate latency induced by SSO
