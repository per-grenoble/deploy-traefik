# Introduction

This project is an [Ansible role](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html). It can be used to automatically perform the deployment of the CDA SSO.

## Features

This role deploys 3 services into a Swarm Cluster:

- `traefik`: an Open Source TCP/HTTP reverse proxy and load balancer that integrates well with Docker Swarm

- `keycloak`: an Open Source Identity and Access Management solution for modern Applications and Services

- `forward-sso`: A minimal forward authentication service that provides OAuth/SSO login and authentication for the traefik reverse proxy/load balancer.

## How it works

![Example](./assets/flow.png)

## Deployment steps

The following actions are performed by the role:

1. Prepare docker configuration and docker secrets (tls cert, tls key). `tags=[traefik]`

1. Prepare docker configuration and docker secrets. `tags=[keycloak`]

1. Create docker network used by all public services. `tags=[traefik]`

1. Create docker network dedicated to keycloak. `tags=[keycloak]`

1. Deploy Traefik as a Docker Swarm service. `tags=[traefik]`

1. Deploy Keycloak as a Docker Swarm service. `tags=[keycloak]`

1. Create a new realm in Keycloak. `tags=[keycloak]`

1. Create a new client associated with new realm. `tags=[sso]`

1. Obtain client secret. `tags=[sso]`

1. Deploy Traefik Forward Auth using client secret. `tags=[sso]`
