# Getting started

## Create your extra variables

Ansible let you override some configuration by using an `extravars` file.

> NOTE: Keycloak does not support wildcard in redirect URIS except for last character, I.E, `https://*.mydomain.com` is not a valid redirect URI while `https://mydomain.com/*` is valid. See [Github Pull Request !3241](https://github.com/keycloak/keycloak/pull/3241)

```yaml
# Configure base domain
domain: dev.compagniedesalpes.fr

# Configure subdomains
keycloak_subdomain: keycloak
traefik_subdomain:wwwww traefik

# Configure docker networks
traefik-network: traefik-network
database_network: keycloak-database-network

# Configure keycloak credentials
keycloak_user: admin
# TODO: Encrypt me using Ansible Vault!
keycloak_password: keycloadk admin password

# Configure traefik entrypoints (optionnal, those are the default values. Entrypoint web and websecure are always created)
traefik_entrypoints:
  gateway:
    address: ":8888"
  mariadb:
    address: ":3306"
  postgres:
    address: ":5432"
```
